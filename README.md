# Latency.GG Demo python probe

This demo probe written in Python 3.9 is not meant for production use.
It is intended as a proof of concept only, for production-ready integrations please contact the Latency.gg team.

```yaml
Compatibility:
    Beacons:
        - v1.0.0
        - ...
        - v1.0.14
    Backend:
      - Demo
```

Usage:
```shell
user@machine:~$ latencygg-demo --help
Usage: latencygg-demo [OPTIONS]

Options:
  --backend-api TEXT  The URL of the Backend (e.g. https://demo.latency.gg)
  --help              Show this message and exit.
  
user@machine:~$ latencygg-demo --backend-api http://demo.latency.gg
oneprovider:
        washingtondc: 76 (~1)
        paris: 8 (~0)
```

Installation (from pip):
```shell
user@machine:~/$ pip install --index-url https://gitlab.com/api/v4/groups/latency.gg/-/packages/pypi/simple latencygg-demo 
```

Installation (from source):
```shell
user@machine:~/$ git clone https://gitlab.com/latency.gg/lgg-probe-python.git
user@machine:~/lgg-probe-python$ cd src
user@machine:~/lgg-probe-python/src$ poetry install
```



Supported ENVs:
```shell
LATENCYGG_BACKEND_API
```